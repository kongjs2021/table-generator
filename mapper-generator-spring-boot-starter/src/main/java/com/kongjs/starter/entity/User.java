package com.kongjs.starter.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Date 2021/2/28
 * @Author kongdechang
 * @Url www.kongjs.com
 * @Email zvxf@outlook.com
 **/


@Table(name = "tb_user")
public class User {
    public User(String name, String pass, Long id) {
        this.name = name;
        this.pass = pass;
        this.id = id;
    }

    public User() {
    }
    @Column(name = "username")
    private String name;
    private String pass;
    @Id
    public Long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
