package com.kongjs.starter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import javax.sql.DataSource;

@SpringBootApplication
public class MapperGeneratorSpringBootStarterApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(MapperGeneratorSpringBootStarterApplication.class, args);
    }

}
