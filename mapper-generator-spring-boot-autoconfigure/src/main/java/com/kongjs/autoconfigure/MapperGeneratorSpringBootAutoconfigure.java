package com.kongjs.autoconfigure;


import com.alibaba.druid.pool.DruidDataSource;
import com.kongjs.autoconfigure.config.ApplicationProperties;
import com.kongjs.autoconfigure.config.DataSourceConfig;
import com.kongjs.autoconfigure.utils.EntityGenerator;
import com.kongjs.autoconfigure.utils.EntityScanner;
import com.kongjs.autoconfigure.utils.SqlRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;


@EnableConfigurationProperties({DataSourceConfig.class, ApplicationProperties.class})
@Configuration
class MapperGeneratorSpringBootAutoconfigure {


    @Autowired
    public DataSourceConfig config;

    private DruidDataSource dataSource = new DruidDataSource();
    @PostConstruct
    public void initDatasource(){
       /* dataSource.setUrl("jdbc:mysql://localhost:3306/mp?serverTimezone=UTC&useSSL=false");
        dataSource.setDbType("com.alibaba.druid.pool.DruidDataSource");
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUsername("root");
        dataSource.setPassword("root");*/
        //System.out.println(dataSourceProperties.getUrl());
        dataSource.setUrl(config.getUrl());
        dataSource.setDbType(String.valueOf(config.getType()));
        dataSource.setDriverClassName(config.getDriverClassName());
        dataSource.setUsername(config.getUsername());
        dataSource.setPassword(config.getPassword());
    }
    @Bean
    public DataSource dataSource() {
        return dataSource;
    }

    @Bean
    public EntityScanner scanner() {
        return new EntityScanner();
    }

    @Bean
    public EntityGenerator generator() {
        return new EntityGenerator();
    }

    @Bean
    public SqlRunner runner() {
        return new SqlRunner();
    }
    @Bean
    public MainAppGenerator mainAppGenerator(){
        return new MainAppGenerator();
    }
}
