package com.kongjs.autoconfigure.config;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.DriverManager;

/**
 * @Date 2021/2/28
 * @Author kongdechang
 * @Url www.kongjs.com
 * @Email zvxf@outlook.com
 **/

@Data
@ConfigurationProperties(prefix = "kj")
public class ApplicationProperties {
    private boolean generator;
    private String basePackage;
}
