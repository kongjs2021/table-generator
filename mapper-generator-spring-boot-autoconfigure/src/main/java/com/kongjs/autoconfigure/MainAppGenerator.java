package com.kongjs.autoconfigure;

import com.kongjs.autoconfigure.config.DataSourceConfig;
import com.kongjs.autoconfigure.utils.EntityGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @Date 2021/3/1
 * @Author kongdechang
 * @Url www.kongjs.com
 * @Email zvxf@outlook.com
 **/

@Component
public class MainAppGenerator {
    @Autowired
    private EntityGenerator generator;
    @Autowired
    private DataSourceConfig config;
    @PostConstruct
    public void mainApp(){
        //System.out.println(config.getPassword());
        generator.create();
    }
}
