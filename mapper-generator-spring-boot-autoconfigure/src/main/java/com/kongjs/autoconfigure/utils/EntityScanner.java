package com.kongjs.autoconfigure.utils;
import com.kongjs.autoconfigure.entity.EntityColumn;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.util.SystemPropertyUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @Date 2021/2/28
 * @Author kongdechang
 * @Url www.kongjs.com
 * @Email zvxf@outlook.com
 **/

@Component
public class EntityScanner implements ResourceLoaderAware {
    private ResourceLoader resourceLoader;
    private ResourcePatternResolver resolver = ResourcePatternUtils.getResourcePatternResolver(resourceLoader);
    private MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourceLoader);

    public void setResourceLoader(@NonNull ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
    public Set<Class<?>> entityScan(String scanPath) {
        Set<Class<?>> classes = new LinkedHashSet<Class<?>>();
        String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX
                .concat(ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(scanPath))
                        .concat("/**/*.class"));
        System.out.println("实体类包扫描路径 = " + packageSearchPath);
        Resource[] resources = new Resource[0];
        try {
            resources = resolver.getResources(packageSearchPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        MetadataReader metadataReader = null;
        for (Resource resource : resources) {
            if (resource.isReadable()) {
                try {
                    metadataReader = metadataReaderFactory.getMetadataReader(resource);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    // 当类型不是抽象类或接口在添加到集合
                    if (metadataReader.getClassMetadata().isConcrete()) {
                        classes.add(Class.forName(metadataReader.getClassMetadata().getClassName()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return classes;
    }
    public Map<String, Set<EntityColumn>> entityMap(String scanPagePath){
        Set<Class<?>> classes = entityScan(scanPagePath);
        //Map<String,Map<String,String>> data = new HashMap<String, Map<String, String>>();
        Map<String,Set<EntityColumn>> em = new HashMap<>(classes.size());
        String tableName;
        Set<EntityColumn> columns = new LinkedHashSet<>();
        //Map<String,String> props = null;
        for (Class<?> entity : classes) {
            boolean entityAnnotationPresent = entity.isAnnotationPresent(Entity.class);
            boolean tableAnnotationPresent = entity.isAnnotationPresent(Table.class);
            if (entityAnnotationPresent){
                Entity e = entity.getAnnotation(Entity.class);
                String value = e.name();
                if (value.isEmpty()){
                    tableName = entity.getSimpleName().toLowerCase();
                }else {
                    tableName = value;
                }
            }else if (tableAnnotationPresent){
                Table t = entity.getAnnotation(Table.class);
                String value = t.name();
                if (value.isEmpty()){
                    tableName = entity.getSimpleName().toLowerCase();
                }else {
                    tableName = value;
                }
            }else {
                tableName = entity.getSimpleName().toLowerCase();
            }
            //System.out.println("类名" + tableName);

            //props = new HashMap<String,String>(entity.getDeclaredFields().length);
            columns = new LinkedHashSet<>(entity.getDeclaredFields().length);
            for (Field declaredField : entity.getDeclaredFields()) {
                String prop = declaredField.getName().toLowerCase();
                String type = declaredField.getType().getSimpleName().toLowerCase();
                //System.out.println("类型:属性 " + type+":"+prop);
                boolean annotationPresent = declaredField.isAnnotationPresent(Column.class);
                if (annotationPresent){
                    Column c = declaredField.getAnnotation(Column.class);
                    String cname = c.name();
                    boolean unique = c.unique();
                    boolean nullable = c.nullable();
                    int length = c.length();
                    //System.out.println(value);
                    if (cname.isEmpty()){
                        //props.put(prop,type);
                        EntityColumn ec = new EntityColumn(prop,unique,nullable,length,type);
                        columns.add(ec);
                    }else {
                        EntityColumn ec = new EntityColumn(cname,unique,nullable,length,type);
                        columns.add(ec);
                        //props.put(cname,type);
                    }
                }else {
                    //props.put(prop,type);
                    EntityColumn ec = new EntityColumn(prop,type);
                    columns.add(ec);
                }
            }
            em.put(tableName,columns);
            //data.put(tableName,props);
        }
     /*   for (Map.Entry<String, Set<EntityColumn>> entry : em.entrySet()) {
            System.out.println(entry);
        }*/
        //System.out.println(em);
        return em;
    }
}
