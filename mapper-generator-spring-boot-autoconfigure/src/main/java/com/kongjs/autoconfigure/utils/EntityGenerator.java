package com.kongjs.autoconfigure.utils;



import com.kongjs.autoconfigure.config.ApplicationProperties;
import com.kongjs.autoconfigure.entity.EntityColumn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

/**
 * @Date 2021/2/28
 * @Author kongdechang
 * @Url www.kongjs.com
 * @Email zvxf@outlook.com
 **/

@Component
public class EntityGenerator {
    @Autowired
    private ApplicationProperties applicationProperties;
    @Autowired
    private SqlRunner sqlRunner;
    @Autowired
    private EntityScanner scanner;
    public void create(){
        Map<String, Set<EntityColumn>> map = scanner.entityMap(applicationProperties.getBasePackage());
        Set<Map.Entry<String, Set<EntityColumn>>> entries = map.entrySet();
        for (Map.Entry<String, Set<EntityColumn>> entry : entries) {
            sqlRunner.create(entry.getKey(),entry.getValue());
        }
    }
    public void delete(){
        Map<String, Set<EntityColumn>> map = scanner.entityMap(applicationProperties.getBasePackage());
        Set<Map.Entry<String, Set<EntityColumn>>> entries = map.entrySet();
        for (Map.Entry<String, Set<EntityColumn>> entry : entries) {
            sqlRunner.delete(entry.getKey());
        }
    }
}
