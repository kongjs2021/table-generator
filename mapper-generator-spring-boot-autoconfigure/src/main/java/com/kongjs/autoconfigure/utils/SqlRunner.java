package com.kongjs.autoconfigure.utils;

import com.alibaba.druid.DbType;
import com.alibaba.druid.pool.DruidDataSource;
import com.kongjs.autoconfigure.config.DataSourceConfig;
import com.kongjs.autoconfigure.entity.ColumnType;
import com.kongjs.autoconfigure.entity.EntityColumn;
import org.apache.commons.dbutils.QueryRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Set;

/**
 * @Date 2021/2/28
 * @Author kongdechang
 * @Url www.kongjs.com
 * @Email zvxf@outlook.com
 **/

@Component
public class SqlRunner {
    @Autowired
    private DataSourceConfig config;

    private DruidDataSource druidDataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl("jdbc:mysql://localhost:3306/mp?serverTimezone=UTC&useSSL=false");
        dataSource.setDbType("com.alibaba.druid.pool.DruidDataSource");
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
 /*       dataSource.setUrl(config.getUrl());
        dataSource.setDbType(DbType.mysql);
        dataSource.setDriverClassName(config.getDriverClassName());
        dataSource.setUsername(config.getUsername());
        dataSource.setPassword(config.getPassword());*/
        return dataSource;
    }

    private QueryRunner qr = new QueryRunner(druidDataSource());
    private String space = " ";
    private String createSql = null;
    private String deleteSql = null;
    private String updateSql = null;

    public void create(String tableName, Set<EntityColumn> ec) {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE " + tableName + " (");
        for (EntityColumn column : ec) {
            String columnName = column.getName();
            String columnType = column.getType();
            int columnLength = column.getLength();
            boolean columnNullable = column.isNullable();
            boolean columnUnique = column.isUnique();
            String sqlColumnType = ColumnType.getSqlColumnType(columnType);
            String sqlColmunNullable = columnNullable ? "NULL ," : "NOT NULL ,";
            String sqlColmunUnique = columnUnique ? "UNIQUE(" + columnName + ") " : "";
            sb.append(columnName).append(space).append(sqlColumnType).append("(").append(columnLength).append(")").append(space).append(sqlColmunNullable).append(sqlColmunUnique).append(space);
        }
        String end = sb.toString().trim().replaceAll(",$", "") + ");";
        createSql = end;
        System.out.println("createSql = " + createSql);
        execute(createSql);
    }


    public void update(String tableName, Set<EntityColumn> ec) {
        execute(updateSql);
    }

    public void delete(String tableName) {
        deleteSql = "DROP TABLE " + tableName + " ;";
        System.out.println("deleteSql = " + deleteSql);
        execute(updateSql);
    }

    private void execute(String sql) {
        try {
            int execute = qr.execute(sql);
            System.out.println(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            System.out.println("sql执行" + sql);
        }
    }
}
