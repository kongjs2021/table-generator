package com.kongjs.autoconfigure.entity;

import lombok.*;

import java.util.Objects;

/**
 * @Date 2021/2/28
 * @Author kongdechang
 * @Url www.kongjs.com
 * @Email zvxf@outlook.com
 **/

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Data
public class EntityColumn {
    private String name;//属性定义了被标注字段在数据库表中所对应字段的名称
    private boolean unique;//属性表示该字段是否为唯一标识，默认为false
    private boolean nullable;//属性表示该字段是否可以为null值，默认为true
    private int length = 10;//属性表示字段的长度，默认为50个字符
    private String type;

    public EntityColumn(String name, boolean unique) {
        this.name = name;
        this.unique = unique;
    }

    public EntityColumn(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public EntityColumn(String name, boolean unique, String type) {
        this.name = name;
        this.unique = unique;
        this.type = type;
    }

    public EntityColumn(String name, boolean unique, boolean nullable) {
        this.name = name;
        this.unique = unique;
        this.nullable = nullable;
    }

    public EntityColumn(String name, boolean unique, boolean nullable, String type) {
        this.name = name;
        this.unique = unique;
        this.nullable = nullable;
        this.type = type;
    }

    public EntityColumn(String name, boolean unique, boolean nullable, int length) {
        this.name = name;
        this.unique = unique;
        this.nullable = nullable;
        this.length = length;
    }

    public EntityColumn(String name, boolean unique, boolean nullable, int length, String type) {
        this.name = name;
        this.unique = unique;
        this.nullable = nullable;
        this.length = length;
        this.type = type;
    }
}
