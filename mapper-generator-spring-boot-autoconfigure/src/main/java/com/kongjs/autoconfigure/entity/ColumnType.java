package com.kongjs.autoconfigure.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.*;

/**
 * @Date 2021/3/1
 * @Author kongdechang
 * @Url www.kongjs.com
 * @Email zvxf@outlook.com
 **/

public class ColumnType {
    private static Map<String, String> map = new  HashMap<>();
    public static String getSqlColumnType(String columnType) {
        map.put("string","varchar");
        map.put("char","char");
        map.put("int","int");
        map.put("integer","int");
        map.put("boolean","tinyint");
        map.put("long","bigint");
        map.put("object","int");
        return map.get(columnType);
    }
}
